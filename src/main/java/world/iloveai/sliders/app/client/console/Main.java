package world.iloveai.sliders.app.client.console;

import bittech.lib.utils.Utils;

public class Main {

	public static void main(String[] args) {

		System.out.print("\033[H\033[2J");
		System.out.flush();
		System.out.println("HW");
		Utils.sleep(1000);
		System.out.println("HW2");

		System.out.print("\033[H\033[2J");
		// Ustawienie koloru tekstu na szary
		System.out.print("\033[90m");

		// Wydrukowanie tekstu
		System.out.println("To jest szary tekst.");

		// Resetowanie koloru do domyślnego
		System.out.print("\033[0m");

		// Wydrukowanie tekstu w domyślnym kolorze
		System.out.println("To jest tekst w domyślnym kolorze.");

		WebSocketConnector srv = WebSocketConnector.asAnonymousClient("ws://localhost:3214/api");

		ClientApp clientApp = new ClientApp(srv);
//		Utils.sleep(10000);
		clientApp.run();

	}

}
