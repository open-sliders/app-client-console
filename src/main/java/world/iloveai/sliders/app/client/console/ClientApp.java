package world.iloveai.sliders.app.client.console;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import bittech.lib.utils.Require;
import bittech.lib.utils.exceptions.StoredException;
import world.iloveai.lib.sliders.data.Column;
import world.iloveai.lib.sliders.data.GuiData;
import world.iloveai.lib.sliders.data.Slider;

public class ClientApp {

	private static ServerConnector serverConnector;
	private static Data data = new Data();
	private static SliderPrinter sliderPrinter = new SliderPrinter(data);

	public ClientApp(WebSocketConnector srv) {
		serverConnector = new ServerConnector(Require.notNull(srv, "srv"));
	}

	public void sayHello() {
		serverConnector.sayHello("http://localhost?max_stage=FULL_PAGE&loading_delay_msec=30&disable_stage_check=true");
	}

	private void crateUserInteractionHandler() {
		UserInteraction userInteraction = new UserInteraction();
		userInteraction.onElementSelected((id) -> serverConnector.elementSelected(id));
		userInteraction.listenForElementSelection();
	}

	public void run() {
		crateUserInteractionHandler();
		sayHello();
		int c = 0;
		while (true) {
			System.out.println("---------- " + c + " : WAITING");
			waitForNewGuiData();
			System.out.println("---------- " + c + " : UPDATEING");
			updateGui();
			c++;
		}
	}

	private void updateGui() {
		try {
			Require.notNull(data.currentGuiData.activeColumnId, "activeColumnId");
			Column activeColumn = data.currentGuiData.columns.get(data.currentGuiData.activeColumnId);
			if (activeColumn == null) {
				throw new Exception("Serwer podesłał activeColumnId, ale nigdy nie wysłał kolumny o tym ID");
			}
			// Na podstawie aktywnej kolumny wyszukujemy wszystkie slajdery a dla każdego z
			// nich
			// Dodatkowo zapamiętujemy elementId, który ma być aktywny
			List<Pair<Slider, Long>> sliders = loadSliders(activeColumn);
			sliderPrinter.printSliders(sliders);
		} catch (Exception e) {
			throw new StoredException("Failed to update gui", e);
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param activeColumn
	 * @return listę sliderów wraz z id aktywnego elementu dla każdego ze sliderów
	 */
	private List<Pair<Slider, Long>> loadSliders(Column activeColumn) {
		try {
			List<Pair<Slider, Long>> sliders = new LinkedList<>(); // Zachowuje kolejność elementów tak jak zostały dodane
			for (Long elementId : activeColumn.elementsIds) {
				Slider slider = data.elsToSliders.get(elementId);
				if (slider == null) {
					throw new Exception("Serwer w kolumnie o columnId = " + activeColumn.id + " podesłał jako jeden z lementów element o id = " + elementId
							+ ". Niestety, nie wiemy jaki slider do tego dopasować. Serwer nie wysłał nigdy wcześniej slidera, który ma taki elementId w zawartości.");
				}
				sliders.add(Pair.of(slider, elementId));
			}
			return sliders;
		} catch (Exception e) {
			throw new StoredException("Failed to load sliders", e);
		}

	}

	private void waitForNewGuiData() {
		GuiData newGuiData = serverConnector.waitForNewGuiData();
		data.newGuiDataReceived(newGuiData);
	}

}
