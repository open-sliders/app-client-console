package world.iloveai.sliders.app.client.console;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import bittech.lib.utils.Require;
import bittech.lib.utils.exceptions.StoredException;
import world.iloveai.lib.sliders.data.Element;
import world.iloveai.lib.sliders.data.Slider;

public class SliderPrinter {

	String separator = " | ";
	String gradiencik = "...";
	Data data;

	public SliderPrinter(Data data) {
		this.data = Require.notNull(data, "data");
	}

	private void printCompletedSlider(List<Element> sliderElements, Slider slider, long activeElementId) {
		StringBuilder sb = new StringBuilder();

		int activeElementLeftPos = -1;
		int activeElementLength = -1;

		Iterator<Element> it = sliderElements.iterator();
		if (sliderElements.get(0).id == activeElementId) {
			sb.append(separator);
		}
		while (it.hasNext()) {
			Element el = it.next();
			String content = el.id + ": " + el.contentHtml;
			if (el.id == activeElementId) {
				activeElementLeftPos = sb.length();
				activeElementLength = content.length();
			}
			sb.append(content);
			if (it.hasNext() || el.id == activeElementId) {
				sb.append(separator);
			}
		}

		if (activeElementLeftPos == -1) {
			throw new StoredException(
					"Nie znaleziono w sliderze aktywnego elementu. Element id = " + activeElementId + ". Slider element id: " + slider.sliderInfo.id, null);
		}

		calcPosAndPrint(slider, sb.toString(), activeElementLeftPos, activeElementLength);

	}

	/**
	 * W otrzymanym sliderContent, początek elementu aktywnego znajduje się na
	 * pozycji
	 * 
	 * @param slider
	 * @param sliderContent        - content całego slideru
	 * @param activeElementLeftPos - index w stringu sliderContent, gdzie zaczyna
	 *                             sie element aktywny w sliderze
	 * @param activeElementLength  - ilość znaków, jakie zawiera element aktywny
	 */
	private void calcPosAndPrint(Slider slider, String sliderContent, int activeElementLeftPos, int activeElementLength) {
		int screenWidth = 80; // 80 znaków
		int externalPos = screenWidth * slider.sliderInfo.handleExternalPercent / 100;
		int internalPos = activeElementLeftPos + (activeElementLength * slider.sliderInfo.handleInternalPercent / 100);
		int offset = externalPos - internalPos;
//		if (offset < 0) {
//			System.err.println("Warning - początek slidera jest ucięty bo zaczyna się wcześniej niż ekran");
//		}
//		if (offset + sliderContent.length() > screenWidth - 1) {
//			System.err.println("Warning - koniec slidera wystaje poza ekran");
//		}

		printString(sliderContent, offset);
	}

	private void printString(String content, int offset) {
		String toPrint = null;
		if (offset > 0) {
			toPrint = " ".repeat(offset) + content;
		} else if (offset < 0) {
			toPrint = content.substring(-offset); // utnij tekst z przodu o długość "diff".
		} else {
			toPrint = content;
		}
		System.out.println(toPrint);
	}

	public void printSliders(List<Pair<Slider, Long>> sliders) {
		System.out.print("\033[H\033[2J");
		System.out.flush();
		for (Pair<Slider, Long> pair : sliders) {
			printSlider(pair.getLeft(), pair.getRight());
		}
	}

	private void printSlider(Slider slider, long activeElementId) {
		List<Element> sliderElements = new LinkedList<>();
		for (Long elementId : slider.elementsIds) {
			Element element = data.currentGuiData.elements.get(elementId);
			if (element == null) {
				printIncompletedSlider(slider, activeElementId);
				return;
			}
			sliderElements.add(element);
		}
		printCompletedSlider(sliderElements, slider, activeElementId);
	}

	private void printIncompletedSlider(Slider slider, long activeElementId) {

		Element activeEl = data.currentGuiData.elements.get(activeElementId);
		StringBuilder sliderContent = new StringBuilder();
		int activeElementPos = separator.length();

		if (slider.elementsIds.get(0) != activeElementId) {
			sliderContent.append(gradiencik);
			activeElementPos += gradiencik.length();
		}

		String activeElementContent = activeEl.id + " : " + activeEl.contentHtml;

		sliderContent.append(separator + activeElementContent + separator);
		if (slider.elementsIds.get(slider.elementsIds.size() - 1) != activeElementId) {
			sliderContent.append(gradiencik);
		}

		calcPosAndPrint(slider, sliderContent.toString(), activeElementPos, activeElementContent.length());
	}

}
