package world.iloveai.sliders.app.client.console;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import bittech.lib.utils.Require;
import bittech.lib.utils.Utils;
import bittech.lib.utils.exceptions.StoredException;
import bittech.lib.utils.json.JsonBuilder;
import world.iloveai.lib.cmd.Listener;
import world.iloveai.lib.cmd.commands.Command;
import world.iloveai.lib.cmd.commands.ErrorResponse;
import world.iloveai.lib.cmd.std.NoDataResponse;
import world.iloveai.lib.sliders.cmd.UpdateGuiCommand;

public class AllCommandsListener implements Listener {

	private static class CatchedCommand {
		Command<?, ?> command;
	}

	private final List<CatchedCommand> receivedCommands = new LinkedList<>();
	private final Map<Command<?, ?>, CatchedCommand> underExecution = new ConcurrentHashMap<>();

	private synchronized CatchedCommand check(Class<?> classOfT, String contains, boolean regex) {
		for (CatchedCommand cc : receivedCommands) {
			if (cc.command.getClass().equals(classOfT)) {
				if (!regex && JsonBuilder.build().toJson(cc.command).contains(contains)) {
					receivedCommands.remove(cc);
					return cc;
				}
				if (regex && JsonBuilder.build().toJson(cc.command).matches(contains)) {
					receivedCommands.remove(cc);
					return cc;
				}
			}
		}
		return null;
	}

	private synchronized void printAllWaiting() {
		for (CatchedCommand cc : receivedCommands) {
			System.out.println(JsonBuilder.build().toJson(cc.command));
		}
	}

	private synchronized void addToList(CatchedCommand cm) {
		receivedCommands.add(cm);
	}

	public <T extends Command<?, ?>> T grabIfExists(Class<T> classOfT, String subStr, boolean regex) {
		CatchedCommand cm = check(classOfT, subStr, regex);

		if (cm != null) {
			underExecution.put(cm.command, cm);
			return (T) cm.command;
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends Command<?, ?>> T waitFor(Class<T> classOfT, String subStr, long timeout, boolean regex) {
		try {
			long timeountTime = System.currentTimeMillis() + timeout;

			while (timeountTime > System.currentTimeMillis()) {

				T ret = grabIfExists(classOfT, subStr, regex);
				if (ret != null) {
					return ret;
				}
				Utils.sleep(10);
			}
			throw new Exception("Timeout. No command received in next " + timeout + " miliseconds", null);
		} catch (Exception ex) {
			printAllWaiting();
			throw new StoredException("Waiting for '" + classOfT + "' failed.", ex);
		}
	}

	public <T extends Command<?, ?>> void noMoreCommand(Class<T> classOfT, String subStr, long timeout, boolean regex) {

		Utils.sleep((int) timeout);

		CatchedCommand cm = check(classOfT, subStr, regex);

		if (cm != null) {
			throw new RuntimeException("Received command, but it should not: " + Utils.toJson(cm), null);
		}

	}

	public void submit(Command<?, ?> command) {
		CatchedCommand cm = underExecution.get(command);
		Require.notNull(cm, "catchedCommand");
		synchronized (cm) {
			System.out.println("Unlocked: " + command.type);
			cm.notify();
		}
	}

	public void submitErrorToAll(ErrorResponse errorResponse) {
		for (CatchedCommand cm : underExecution.values()) {
			synchronized (cm) {
				cm.command.error = errorResponse;
				cm.notify();
			}
		}
	}

	@Override
	public Class<?>[] getListeningCommands() {
		return new Class<?>[] { UpdateGuiCommand.class };
	}

//	@Override
//	public String[] getListeningServices() {
//		return null;
//	}

	@Override
	public void commandReceived(String fromServiceName, Command<?, ?> command) throws StoredException {
		try {
			CatchedCommand cm = new CatchedCommand();
			cm.command = command;

			if (command instanceof UpdateGuiCommand) {
				((UpdateGuiCommand) command).response = new NoDataResponse();
			}

			synchronized (cm) {
				addToList(cm);
			}
		} catch (Exception ex) {
			new StoredException("Command received failed", ex);
		}

	}

	@Override
	public void responseSent(String serviceName, Command<?, ?> command) {
		// Nothing here
	}

}
