package world.iloveai.sliders.app.client.console;

import java.util.HashMap;
import java.util.Map;

import bittech.lib.utils.Utils;
import world.iloveai.lib.sliders.data.GuiData;
import world.iloveai.lib.sliders.data.Slider;

public class Data {

	public GuiData currentGuiData = new GuiData(null, 0);
	public Map<Long, Slider> elsToSliders = new HashMap<>();

	private void addNewElementsToMap(GuiData newGuiData) {
		for (Slider slider : newGuiData.sliders.values()) {
			for (Long elementId : slider.elementsIds) {
				elsToSliders.put(elementId, slider);
			}
		}
	}

	public void newGuiDataReceived(GuiData newGuiData) {
		Utils.prn("CurrentGui data: ", currentGuiData);
		Utils.prn("Plus new gui data: ", newGuiData);
		currentGuiData.plus(newGuiData); // mergujemy stare guiData z nowym
		Utils.prn("Equals: ", currentGuiData);
		addNewElementsToMap(newGuiData); // Update elementsToSlidersMap
	}

}
