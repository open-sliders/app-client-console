package world.iloveai.sliders.app.client.console;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import bittech.lib.utils.Require;
import bittech.lib.utils.Try;
import bittech.lib.utils.Utils;

public class UserInteraction implements AutoCloseable {

	Scanner scanner;
	ExecutorService executorService;
	Consumer<Long> consumer;

	public void listenForElementSelection() {
		Require.notNull(consumer, "consumer");
		executorService = Executors.newSingleThreadExecutor();
		executorService.submit(() -> {
			scanner = new Scanner(System.in);
			while (true) {
				try {
					System.out.print("Wprowadź identyfikator elementu: ");
					String polecenie = scanner.nextLine();
					if ("exit".equals(polecenie)) {
						break;
					}
					long elementId = Long.parseLong(polecenie);
					if (elementId < 0) {
						System.out.println("Błąd: Identyfikator elementu nie może być ujemny.");
						continue;
					}
					consumer.accept(elementId);
				} catch (NumberFormatException e) {
					System.out.println("Błąd: Wprowadzony tekst nie jest liczbą.");
					scanner.nextLine(); // Czyści bufor
				} catch (Exception e) {
					System.out.println("Nieoczekiwany błąd: " + e.getMessage());
				}
			}
		});
	}

	public void onElementSelected(Consumer<Long> consumer) {
		this.consumer = Require.notNull(consumer, "consumer");
	}

	@Override
	public void close() {
		Try.printIfThrown(() -> scanner.close());
		Try.printIfThrown(() -> executorService.shutdownNow());
	}

	public static void main(String[] args) {
		try (UserInteraction selector = new UserInteraction()) {
			selector.onElementSelected((id) -> {
				System.out.println("Kliknales w: " + id);
			});

			selector.listenForElementSelection();
			while (true) {
				Utils.sleep(5000);
				System.out.println("hhhrrrapp...");
			}

		}
	}

}
