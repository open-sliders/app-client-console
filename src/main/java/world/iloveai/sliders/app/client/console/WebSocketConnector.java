package world.iloveai.sliders.app.client.console;

import bittech.lib.utils.exceptions.StoredException;
import bittech.lib.utils.json.JsonBuilder;
import world.iloveai.lib.cmd.commands.Command;
import world.iloveai.lib.ws.WsCmd;
import world.iloveai.lib.ws.WsConfig;

public class WebSocketConnector implements AutoCloseable {

	WsCmd webSocketClient;
	AllCommandsListener cmdsListener; // TODO: Oddzielny listener dla aplikacji

	public static WebSocketConnector asAnonymousClient(String serverUri) {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("{");
			sb.append("    \"wsClient\": {");
			sb.append("      \"connections\":[");
			sb.append("        {\"peerName\":\"srv\",");
			sb.append("         \"serverUri\":\"").append(serverUri).append("\"");
			sb.append("        }");
			sb.append("      ]");
			sb.append("    }");
			sb.append("  }");

			WsConfig conf = JsonBuilder.build().fromJson(sb.toString(), WsConfig.class);
			return new WebSocketConnector(conf);
		} catch (Exception e) {
			throw new StoredException("Failed to crate WebSocketConnector", e);
		}
	}

	public static WebSocketConnector asClient(String serverUri, String login) {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("{");
			sb.append("    \"wsClient\": {");
			sb.append("      \"connections\":[");
			sb.append("        {\"peerName\":\"srv\",");
			sb.append("         \"serverUri\":\"").append(serverUri).append("\",");
			sb.append("         \"usr\":\"").append(login).append("\", ");
			sb.append("         \"pwd\":\"pwd345\"");
			sb.append("        }");
			sb.append("      ]");
			sb.append("    }");
			sb.append("  }");

			WsConfig conf = JsonBuilder.build().fromJson(sb.toString(), WsConfig.class);
			return new WebSocketConnector(conf);
		} catch (Exception e) {
			throw new StoredException("Failed to crate WebSocketConnector", e);
		}

	}

//	public static WebSocketConnector asServer(int port) {
//		StringBuilder sb = new StringBuilder();
//		sb.append("{");
//		sb.append("    \"wsServer\": {");
//		sb.append("      \"port\":").append(port).append(",");
//		sb.append("      \"users\":[");
//		sb.append("        {\"login\":\"srv\", \"pwd\":\"pwd\"}");
//		sb.append("      ]");
//		sb.append("    }");
//		sb.append("  }");
//
//		WsConfig conf = JsonBuilder.build().fromJson(sb.toString(), WsConfig.class);
//		return new WebSocketConnector(conf);
//	}

	private WebSocketConnector(WsConfig conf) {
		try {
			webSocketClient = WsCmd.fromConfig(conf);
			cmdsListener = new AllCommandsListener();
			webSocketClient.addListener(cmdsListener);
//			webSocketClient.newConnection("srv", serverUri, "wsClient", "pwd");
			webSocketClient.start();
			webSocketClient.waitForConnectionEstablished("srv");
		} catch (Exception ex) {
			throw new StoredException("Cannot create WebSocketConnector", ex);
		}
	}

	public void send(String peerName, Command<?, ?> command) {
		webSocketClient.send("srv", command);
	}

	public void submit(Command<?, ?> command) {
		cmdsListener.submit(command);
	}

	public <T extends Command<?, ?>> T waitForRegex(Class<T> classOfT, String regex, long timeout) {
		return cmdsListener.waitFor(classOfT, regex, timeout, true);
	}

	public <T extends Command<?, ?>> T waitFor(Class<T> classOfT, String contains, long timeout) {
		return cmdsListener.waitFor(classOfT, contains, timeout, false);
	}

	public <T extends Command<?, ?>> T waitFor(Class<T> classOfT, long timeout) {
		return cmdsListener.waitFor(classOfT, "", timeout, false);
	}

	public <T extends Command<?, ?>> T grabIfExists(Class<T> classOfT) {
		return cmdsListener.grabIfExists(classOfT, "", false);
	}

	public <T extends Command<?, ?>> void noMoreCommandWithRegex(Class<T> classOfT, String regex, long timeout) {
		cmdsListener.noMoreCommand(classOfT, regex, timeout, true);
	}

	public <T extends Command<?, ?>> void noMoreCommand(Class<T> classOfT, String contains, long timeout) {
		cmdsListener.noMoreCommand(classOfT, contains, timeout, false);
	}

	public <T extends Command<?, ?>> void noMoreCommands(Class<T> classOfT, long timeout) {
		cmdsListener.noMoreCommand(classOfT, "", timeout, false);
	}

	@Override
	public void close() {
		webSocketClient.close();
		System.out.println("Tester closed");
	}
}
