package world.iloveai.sliders.app.client.console;

import org.junit.Assert;

import bittech.lib.utils.Require;
import bittech.lib.utils.Utils;
import world.iloveai.lib.sliders.cmd.ElementSelectedCommand;
import world.iloveai.lib.sliders.cmd.HelloCommand;
import world.iloveai.lib.sliders.cmd.UpdateGuiCommand;
import world.iloveai.lib.sliders.data.GuiData;
import world.iloveai.lib.sliders.data.GuiStage;

public class ServerConnector {

	WebSocketConnector srv;

	public ServerConnector(WebSocketConnector srv) {
		this.srv = Require.notNull(srv, "srv");
	}

	public void sayHello(String uri) {
		{
			HelloCommand cmd = new HelloCommand(uri);
			srv.send("", cmd); // Wysyła request. Serwer powinien odesłać Response, co u mnie oznacza, że:
			Assert.assertNull(cmd.getError()); // .. Serwer nie odesłał errora
			Assert.assertNotNull(cmd.getResponse()); // .. Serwer odesłał response

			Utils.prn("Settings", cmd.response.settings);
		}
	}

	public void elementSelected(long id) {
		System.out.println("Wybrales element: " + id);
		ElementSelectedCommand cmd = new ElementSelectedCommand(id, GuiStage.FULL_PAGE);
		srv.send("", cmd);
		Assert.assertNull(cmd.getError()); // .. Serwer nie odesłał errora
		Assert.assertNotNull(cmd.getResponse()); // .. Serwer odesłał response
	}

	public GuiData waitForNewGuiData() {
		UpdateGuiCommand cmd = srv.waitFor(UpdateGuiCommand.class, 10000000);
		return cmd.getRequest().guiData;
	}
}
